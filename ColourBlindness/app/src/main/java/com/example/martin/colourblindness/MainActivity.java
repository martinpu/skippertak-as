package com.example.martin.colourblindness;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * Front page of the application explaining what the application is used for
 * and it has a button to start the test.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    /**
     * Button to start the test
     * Suppressed unused view due to use of onclick in xml
     */
    @SuppressWarnings("unused")
    public void startTest(View view) {
        Intent intent = new Intent(this, Demo.class);
        startActivity(intent);
    }
}
