package com.example.martin.colourblindness;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ViewFlipper;

public class Demo extends Activity {

    private ViewFlipper viewFlipper;

    private int current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);

        viewFlipper = findViewById(R.id.viewFlipperDemo);
        this.current = 1;
        Log.d("Test", "The current plate: " + this.current);

    }
    //Suppressed unused view due to use of onclick in xml
    @SuppressWarnings("unused")
    public void next(View view) {
        if(this.current < 8) {
            viewFlipper.showNext();
            this.current++;
        } else {
            Intent intent = new Intent(this, TestActivityHrr.class);
            startActivity(intent);
        }
    }
    //Suppressed unused view due to use of onclick in xml
    @SuppressWarnings("unused")
    public void prev(View view) {
        viewFlipper.showPrevious();
        this.current--;
    }
}
