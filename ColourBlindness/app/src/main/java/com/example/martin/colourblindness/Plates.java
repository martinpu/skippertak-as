package com.example.martin.colourblindness;

/**
 * Class to set values to the plates both for the buttons and the touch buttons in the
 * middle of the screen.
 */

class Plates {
    private final boolean[][] uAnswers;
    private final boolean[][] uTouchAnswers;
    private float byRes;
    private float rgRes;
    //boolean values(answers) corresponding to symbols: Circle - Up/down triangle - xCross - sideways triangle
    // for every plate ranging 5 - 24
    private final boolean pAnswers[][] = {
            {true, false, true, false},    //plate 5 - B-Y start
            {true, true, false, false},
            {false, false, true, true},    //plate 7 - R-G start
            {true, false, false, true},
            {true, false, false, false},
            {false, false, true, false},
            {true, true, false, false},    //Plate 11 - protan deutan start
            {false, false, true, false},
            {false, true, false, false},
            {true, false, true, false},
            {true, false, true, false},
            {true, false, false, true},
            {true, false, false, true},
            {false, false, true, true},
            {true, false, true, false},
            {true, true, false, false},
            {false, true, true, false},    // plate 21 - Tritan and tetartan start
            {true, false, true, false},
            {true, true, false, false},
            {false, false, true, true}
    };
    //boolean values(touch answers) corresponding to display regions and where there is a symbol place: top left - top right - bottom left - bottom right
    // for every plate ranging 5 - 24
    private final boolean tAnswers[][] = {
            {true, true, false, false},    //plate 5 - B-Y start
            {true, false, false, true},
            {true, true, false, false},    //plate 7 - R-G start
            {true, true, false, false},
            {true, false, false, false},
            {false, false, false, true},
            {true, false, true, false},    //Plate 11 - protan deutan start
            {false, true, false, false},
            {true, false, false, false},
            {false, true, true, false},
            {true, false, false, true},
            {false, false ,true, true},
            {true, true, false, false},
            {false, true, true, false},
            {false, true, true, false},
            {true, false, false, true},
            {false, true, false, true},    // plate 21 - Tritan and tetartan start
            {true, true, false, false},
            {true, false, false, true},
            {false, true, true, false},

    };
    //Array that holds different symbols in touch answers: 0 - circle, 1 - up/down triangle, 2 X-cross, 3 side triangle, 9 Null or no symbol in the plate
    private final int tSymbolCircle[]     = {0, 0, 9, 0, 0, 9, 2, 9, 9, 2, 0, 3, 0, 9, 2, 0, 9, 0, 0, 9};
    private final int tSymbolTriangle[]   = {9, 3, 9, 9, 9, 9, 0, 9, 0, 9, 9, 9, 9, 9, 9, 3, 3, 9, 3, 9};
    private final int tSymbolCross[]      = {1, 9, 0, 9, 9, 3, 9, 1, 9, 1, 3, 9, 9, 2, 1, 9, 1, 1, 9, 1};
    private final int tSymbolSideTri[]    = {9, 9, 1, 1, 9, 9, 9, 9, 9, 9, 9, 2, 1, 1, 9, 9, 9, 9, 9, 2};
    //arrays that represent the touch interface deficiency answers
    private final int protanTouchAnswers[]     = {2, 9, 0, 2, 3, 2, 0, 1, 1, 0};
    private final int deutanTouchAnswers[]     = {0, 1, 9, 1, 0, 3, 1, 2, 2, 3};
    private final int tritanTouchAnswers[]     = {3, 1, 0, 2};
    private final int tertartanTouchAnswers[]  = {1, 0, 3, 1};
    //Diagnostic series analysis arrays for types: Protan, Deutan, Tritan, Tertaran
    //values in arrays represents the columns in pAnswers 2d Array - 9 is outside of cols bound = NULL or no answer
    private final int protanAnswers[]     = {0, 9 , 1, 0, 2, 3, 0, 3, 2, 0};
    private final int deutanAnswers[]     = {1, 2, 9, 2, 0, 0, 3, 2, 0, 1};
    private final int tritanAnswers[]     = {1, 2, 0, 3};
    private final int tertartanAnswers[]  = {2, 0, 1, 2};

    Plates(boolean uAnswers[][], boolean uTouchAnswers[][]){
        this.uAnswers = uAnswers;
        this.uTouchAnswers = uTouchAnswers;
    }

    /**
     * Calculates the protan deficiency and returns its value
     * @return string of deficiency strength
     */
    public String getProtan(){
        return getProDeuDefect(getProtanResult());
    }
    /**
     * Calculates the Deutan deficiency and returns its value
     * @return string of deficiency strength
     */
    public String getDeutan(){
        return getProDeuDefect(getDeutanResult());
    }
    /**
     * Calculates the Tritan deficiency and returns its value
     * @return string of deficiency strength
     */
    public String getTritan(){
        return getTriTerDefect(getTritanResult());
    }
    /**
     * Calculates the Tetartan deficiency and returns its value
     * @return string of deficiency strength
     */
    public String getTetartan(){
        return getTriTerDefect(getTertartanResult());
    }
    /**
     *
     * @return string of deficiency strength
     */
    public String getBY(){
        if(byRes < 2)
            return "Deficiency detected";
        return "No signs of deficiency";
    }
    public String getRG(){
        if(rgRes < 3)
            return "Deficiency detected";
        return "No signs of deficiency";
    }

    /**
     * returns the protan deficiency
     * @return point result
     */
   private int getProtanResult(){
       return loopDeuProtan(protanAnswers, protanTouchAnswers);
   }
    /**
     * returns the deutan deficiency
     * @return point result
     */
    private int getDeutanResult(){
       return loopDeuProtan(deutanAnswers, deutanTouchAnswers);
   }

    /**
     * compares users input answers against the result answers
     * @param deuProtan either deutan/protan multiple choice plate answers
     * @param deuProtanTouch either deutan/protan touch plate answers
     * @return result in points
     */
   private int loopDeuProtan(int deuProtan[], int deuProtanTouch[]){
       int result = 0;
       int col = 0;
       for(int i = 6; i < 16; i++){
           if(deuProtan[col] != 9 || deuProtanTouch[col] != 9){
               if(pAnswers[i][deuProtan[col]] == uAnswers[i][deuProtan[col]] && tAnswers[i][deuProtanTouch[col]] == uTouchAnswers[i][deuProtanTouch[col]])
                   result++;
           }
           col++;
       }
       return result;
   }

    /**
     * returns the tritan deficiency
     * @return point result
     */
    private int getTritanResult(){
        return loopTriTertan(tritanAnswers, tritanTouchAnswers);
   }
    /**
     * returns the tertartan deficiency
     * @return point result
     */
    private int getTertartanResult(){
        return loopTriTertan(tertartanAnswers, tertartanTouchAnswers);
    }
    /**
     * compares users input answers against the result answers
     * @param triTartanAnswers either tritan/tertartan multiple choice plate answers
     * @param triTartanTouch either tritan/tertartan touch plate answers
     * @return result in points
     */
    private int loopTriTertan(int[] triTartanAnswers, int[] triTartanTouch) {
        int result = 0;
        int col = 0;
        for(int i = 16; i < 20; i++){
            if(pAnswers[i][triTartanAnswers[col]] == uAnswers[i][triTartanAnswers[col]] && tAnswers[i][triTartanTouch[col]] == uTouchAnswers[i][tritanTouchAnswers[col]])
                result++;
            col++;
        }
        return result;
    }


    /**
     * Gets the kind of defect for either protan/deutan
     * @param res result of either protan/deutan
     * @return value of deficiency
     */
    private String getProDeuDefect(int res){
        String tmp;
        if(res < 2)
            tmp = "Strong deficiency detected";
        else if(res < 5)
            tmp = "Medium deficiency detected";
        else if(res < 9)
            tmp = "Mild deficiency detected";
        else
            tmp = "No signs of deficiency";
        return tmp;
    }
    /**
     * Gets the kind of defect for either tritan/tertartan
     * @param res result of either tritan/tertartan
     * @return value of deficiency
     */
    private String getTriTerDefect(int res){
        String tmp;
        if(res < 2)
            tmp = "Strong deficiency detected";
        else if(res < 4)
            tmp = "Medium deficiency detected";
        else
            tmp = "No signs of deficiency";
        return tmp;
    }

    /**
     *Compares users input to the plates answers
     * @return point result
     */
    public float compareTouchUserInput(){
        float result = 0;
        for(int i = 0; i < 20; i++){
            for(int j = 0; j < 4; j++){
                //Check if the plate column is true - we are not counting points when user and plate is false
                if (!uAnswers[i][j]) {
                    continue;
                }
                if (uAnswers[i][j] == pAnswers[i][j]) {
                    switch (j) {
                        case 0: {
                            if (tSymbolCircle[i] != 9){
                                if(uTouchAnswers[i][tSymbolCircle[i]])
                                    if(i < 2)
                                        byRes += 0.5;
                                    else if(i < 6)
                                        rgRes += 0.5;
                                    result += 0.5;
                            }
                        }break;
                        case 1: {
                            if (tSymbolTriangle[i] != 9){
                                if(uTouchAnswers[i][tSymbolTriangle[i]])
                                    if(i < 2)
                                        byRes += 0.5;
                                    else if(i < 6)
                                        rgRes += 0.5;
                                    result += 0.5;
                            }
                        }break;
                        case 2: {
                            if (tSymbolCross[i] != 9){
                                if(uTouchAnswers[i][tSymbolCross[i]])
                                    if(i < 2)
                                        byRes += 0.5;
                                    else if(i < 6)
                                        rgRes += 0.5;
                                    result += 0.5;
                            }
                        }break;
                        case 3: {
                            if(tSymbolSideTri[i] != 9){
                                if(uTouchAnswers[i][tSymbolSideTri[i]])
                                    if(i < 2)
                                        byRes += 0.5;
                                    else if(i < 6)
                                        rgRes += 0.5;
                                    result += 0.5;
                            }
                        }break;
                        default:
                            System.out.println("");
                    }
                }
            }
        }
    return result;
    }
}
