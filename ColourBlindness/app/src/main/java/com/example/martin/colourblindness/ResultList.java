package com.example.martin.colourblindness;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Class to display the results from the test. It is given in text views with description
 * no defect / mild / strong in the different categories protan, deutan, tritan and tetartan.
 */
public class ResultList extends AppCompatActivity{
    // Bundle containing the array with results from TestActivityHrr.
    final private TextView[] defect = new TextView[7];
    final private TextView[] deficiencyTextView = new TextView[4];
    final private String[] deficiencyText = new String[4];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_list);
        initializeView();
        displayDefects();
    }
    //Suppressed unused view due to use of onclick in xml
    @SuppressWarnings("unused")
    public void returnToStart(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    /**
     * Checks if user has any defects, if populate and display the defect and its associated strength
     */
    private void displayDefects(){
        Bundle bundle;
        // Get the array sent from TestActivityHrr.
        bundle = this.getIntent().getExtras();
        assert bundle != null;
        String[] array = bundle.getStringArray("answers");
        // Set the text of the text views on result_list will the given score from the test.
        assert array != null;
        for(int i = 0; i < 7; i++){
            if(i < 4) {
                if (!deficiencyDetected(array[i]))
                    deficiencyTextView[i].setText(deficiencyText[i]);
            }
            defect[i].append(array[i]);
        }
    }

    /**
     * Checks if a deficiency is detected or not
     * @param res string of defect
     * @return bool if deficiency is detected
     */
    private boolean deficiencyDetected(String res){
        String parts[] = res.split(" ");
        return parts[0].equals("No");
    }

    /**
     * Initialize textviews
     */
    private void initializeView(){
        defect[0] = findViewById(R.id.protan);
        defect[1] = findViewById(R.id.deutan);
        defect[2] = findViewById(R.id.tritan);
        defect[3] = findViewById(R.id.tetartan);
        defect[4] = findViewById(R.id.blueYellow);
        defect[5] = findViewById(R.id.redGreen);
        defect[6] = findViewById(R.id.fullScore);

        deficiencyTextView[0] = findViewById(R.id.protanTextView);
        deficiencyTextView[1] = findViewById(R.id.deutanTextView);
        deficiencyTextView[2] = findViewById(R.id.tritanTextView);
        deficiencyTextView[3] = findViewById(R.id.tetartanTextView);

        deficiencyText[0] = getResources().getString(R.string.info_text_protan);
        deficiencyText[1] = getResources().getString(R.string.info_text_deutan);
        deficiencyText[2] = getResources().getString(R.string.info_text_tritan);
        deficiencyText[3] = getResources().getString(R.string.info_text_tetartan);
    }
}
