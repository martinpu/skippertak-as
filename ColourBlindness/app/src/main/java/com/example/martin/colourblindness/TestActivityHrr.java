package com.example.martin.colourblindness;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ViewFlipper;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class will include the colour blindness test based on 24 HRR Plates.
 * It will show show different types of colour blindness test plates and find out what kind
 * of colour blindness the user suffers from, if any.
 */
public class TestActivityHrr extends Activity implements View.OnClickListener {
    // Constants
    private static final int DELAY_TIMER = 8000; // Time in millis for timer
    private static final int YELLOW = 0xFFBBAA00;
    private static final int DEFAULT = 0xFFFFFFFF;
    // Variables needed for views and buttons.
    private ViewFlipper viewFlipper;
    private Button circleBtn, exBtn, triangleBtn, triSideBtn, tLeft, tRight, bLeft, bRight, save;
    //Plates checkAnswers = new Plates();
    private final boolean[][] userAnswers = new boolean[20][4];
    private final boolean[][] touchAnswers = new boolean[20][4];
    // Variables needed for current(The current picture/plate shown) and booleans to check
    // if the button is pressed before the next plate.
    private int current;
    private boolean circlePressed, trianglePressed, exPressed, triangleSidePressed, tRightPress, tLeftPress, bRightPress, bLeftPress;
    private int circleCheck, triangleCheck, exCheck, triangleSideCheck, tRightCheck, tLeftCheck, bRightCheck, bLeftCheck;
    {
        circleCheck = triangleCheck = exCheck = triangleSideCheck = tRightCheck = tLeftCheck = bRightCheck = bLeftCheck = 1;
        circlePressed = trianglePressed = exPressed = triangleSidePressed = tRightPress = tLeftPress = bRightPress = bLeftPress = false;
    }
    private Timer timer = new Timer();
    /**
     * On create of the class Testactivity. This will build all the needed views, buttons and
     * variables.
     *
     * @param savedInstanceState the savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hrr_test);

        viewFlipper = findViewById(R.id.viewFlipper);
        this.current = viewFlipper.getDisplayedChild() + 5;
        // Initiate buttons and views.
        circleBtn = findViewById(R.id.circleBtn);
        exBtn = findViewById(R.id.exBtn);
        triangleBtn = findViewById(R.id.triangleBtn);
        triSideBtn = findViewById(R.id.triangleSideBtn);
        save = findViewById(R.id.save);

        tLeft = findViewById(R.id.tLeft);
        tRight = findViewById(R.id.tRight);
        bLeft = findViewById(R.id.bLeft);
        bRight = findViewById(R.id.bRight);

        setTimer(8000);
        /*
          Method to set a onClickListener on the top left quadrant view in the UI. It will also set
          Displays the border on chosen field
         */
        tLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tLeftCheck == 1){
                    tLeftPress = true;
                    tLeft.setBackgroundResource(R.drawable.button_border);
                    tLeftCheck = 0;
                } else {
                    tLeftPress = false;
                    tLeft.setBackgroundResource(0);
                    tLeftCheck = 1;
                }
            }
        });
        /*
          Method to set a onClickListener on the top right quadrant view in the UI. It will also set
          Displays the border on chosen field
         */
        tRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tRightCheck == 1){
                    tRightPress = true;
                    tRight.setBackgroundResource(R.drawable.button_border);
                    tRightCheck = 0;
                } else {
                    tRightPress = false;
                    tRight.setBackgroundResource(0);
                    tRightCheck = 1;
                }
            }
        });
        /*
          Method to set a onClickListener on the bottom left quadrant view in the UI. It will also set
          Displays the border on chosen field
         */
        bLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bLeftCheck == 1){
                    bLeftPress = true;
                    bLeft.setBackgroundResource(R.drawable.button_border);
                    bLeftCheck = 0;
                } else {
                    bLeftPress = false;
                    bLeft.setBackgroundResource(0);
                    bLeftCheck = 1;
                }
            }
        });
        /*
          Method to set a onClickListener on the bottom right quadrant view in the UI. It will also set
          Displays the border on chosen field
         */
        bRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bRightCheck == 1){
                    bRightPress = true;
                    bRight.setBackgroundResource(R.drawable.button_border);
                    bRightCheck = 0;
                } else {
                    bRightPress = false;
                    bRight.setBackgroundResource(0);
                    bRightCheck = 1;
                }
            }
        });

        /*
          Method to set a onClickListener on the circle button in the UI. It will also set
          the circle button to pressed and change its colour.
         */
        circleBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (circleCheck == 1) {
                    circleBtn.getBackground().setColorFilter(YELLOW, PorterDuff.Mode.MULTIPLY);
                    circlePressed = true;
                    circleCheck = 0;
                } else {
                    circleBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
                    circlePressed = false;
                    circleCheck = 1;
                }
            }
        });
        /*
          Method to set a onClickListener on the triangle button in the UI. It will also set
          the triangle button to pressed and change its colour.
         */
        triangleBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (triangleCheck == 1) {
                    triangleBtn.getBackground().setColorFilter(YELLOW, PorterDuff.Mode.MULTIPLY);
                    trianglePressed = true;
                    triangleCheck = 0;
                } else {
                    triangleBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
                    trianglePressed = false;
                    triangleCheck = 1;
                }
            }
        });
        /*
          Method to set a onClickListener on the X button in the UI. It will also set
          the X button to pressed and change its colour.
         */
        exBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (exCheck == 1) {
                    exBtn.getBackground().setColorFilter(YELLOW, PorterDuff.Mode.MULTIPLY);
                    exPressed = true;
                    exCheck = 0;
                } else {
                    exBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
                    exPressed = false;
                    exCheck = 1;
                }
            }
        });
        /*
          Method to set a onClickListener on the side pointed triangle button in the UI. It will also set
          the side pointed triangle button to pressed and change its colour.
         */
        triSideBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (triangleSideCheck == 1) {
                    triSideBtn.getBackground().setColorFilter(YELLOW, PorterDuff.Mode.MULTIPLY);
                    triangleSidePressed = true;
                    triangleSideCheck = 0;
                } else {
                    triSideBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
                    triangleSidePressed = false;
                    triangleSideCheck = 1;
                }
            }
        });
    }

    /**
     * Method to save the answer from the user and send in the next plate. This method also saves
     * the answer into a List for the result to be created.
     * Suppressed unused view due to use of onclick in xml
     */
    @SuppressWarnings("unused")
    public void saveId(View view) {
        killTimer();
        //Add user input to arrays
        boolean arr[] = {circlePressed, trianglePressed, exPressed, triangleSidePressed};
        boolean arr2[] = {tLeftPress, tRightPress, bLeftPress, bRightPress};
        //copy the users answers into touch and multiple choice arrays
        System.arraycopy(arr, 0, userAnswers[this.current - 5], 0, 4);
        System.arraycopy(arr2, 0, touchAnswers[this.current - 5], 0, 4);

        viewFlipper.showNext();
        this.current = viewFlipper.getDisplayedChild() + 5;
        resetPlate();
        if(this.current == 25){
            killTimer();
            showResult();
        }
    }

    /**
     * Passing results from input and showing new intent with score results
     */
    private void showResult() {
        //Compares result
        Plates checkAnswers = new Plates(userAnswers, touchAnswers);
        float res2 = checkAnswers.compareTouchUserInput();

        String protanDefect = checkAnswers.getProtan();
        String deutanDefect = checkAnswers.getDeutan();
        String tritanDefect = checkAnswers.getTritan();
        String tetartanDefect = checkAnswers.getTetartan();
        String bYellow = checkAnswers.getBY();
        String rGreen = checkAnswers.getRG();
        Bundle bundle = new Bundle();
        bundle.putStringArray("answers", new String[]{protanDefect, deutanDefect, tritanDefect, tetartanDefect, bYellow, rGreen, Float.toString(res2)});


        Intent intent = new Intent(this, ResultList.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * Resets the buttons and timer for every plate
     */
    private void resetPlate(){
        // Set Colour back to standard after plate change.
        circleBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
        triangleBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
        exBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
        triSideBtn.getBackground().setColorFilter(DEFAULT, PorterDuff.Mode.MULTIPLY);
        // Remove the border around the touched buttons before next plate is shown.
        tLeft.setBackgroundResource(0);
        tRight.setBackgroundResource(0);
        bLeft.setBackgroundResource(0);
        bRight.setBackgroundResource(0);
        // Set the check back to 1 so that the colour on the button is correct after plate change.
        circleCheck = triangleCheck = exCheck = triangleSideCheck = 1;
        tRightCheck = tLeftCheck = bRightCheck = bLeftCheck = 1;
        // Set buttons unpressed on plate change.
        circlePressed = trianglePressed = exPressed = triangleSidePressed = false;
        tRightPress = tLeftPress = bRightPress = bLeftPress = false;
        // Creates new timer before next plate is shown.
        timer = new Timer();
        setTimer(DELAY_TIMER);

    }
    @Override
    public void onClick(View v) {

    }

    /**
     * Method to set a timer for the plates. The user will have a set time to work with.
     * @param time in ms for the timer
     */
    private void setTimer(int time) {
        if (this.current < 25) {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            save.performClick();
                        }
                    });
                }
            }, time);
        }
    }

    /**
     * Kills the previous timer and removes all canceled timers from the queue.
     */
    private void killTimer(){
        timer.cancel();
        timer.purge();
    }
}

