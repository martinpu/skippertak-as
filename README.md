# README #

## This test is still copyrighted and will only be used for academic purposes! ##

Group: Skippertak AS
Members/Authors: Martin Pukstad. Stian Fenstad, Kristian Sundhaugen and Jakob Fonstad
Supervisor/Idea submitter: Philip John Green

This the the repository for the ColourBlindness App which was made for the course Mobile/Wearable Programming.

# The Main Idea: #

Develop an app that tests the user for their ability to distinguish colour. The app can test for colourblindness
along the typical schemes, as well as test relative sensitivity of the user in general, to be
able to distinguish colours. Using existing colour blindness test apps as a model is advised and
encouraged.

# The Update Idea after correspondanse with supervisor: #

The app will be developed as a version of the HRR (Hardy-Rand-Rittler) test. This will include 24
plates which tests R-G and B-Y defects and more specific Protan, Deutan, Tritan and Tetartan defects.

The test will first have four plates to demonstrate how the test works with explanation. After these
the next two plates are a check on B-Y (Blue � Yellow) defects before the next four plates check for
defects in the R-G (Red � Green). When this is done there will be more specific checks on Protan and
Deutan defects regarding R-G. And to conclude there will be four plates to check Tritan and Tetartan
defects regarding B-Y. These are extreme cases since the colours are very strong. 

# The code: #

MainActivity.java/activity_main.xml:
MainActivity.java with corresponding activity_main.xml is for the front page of the application. This page will display
an explanation on what the test is about, and also works to mention the existing HRR test which is copyrighted. The xml only
contains an  text view and a button. The button takes you to the next activity which is demo.java

Demo.java/demo.xml:
The demo.xml file contains two buttons "PREVIOUS" and "NEXT" for the user to flip through the demonstration plates of the test.
The test is set up so that the four first plates in the series are only for a demonstration and will not be included in the score.
The xml file also has a ViewFlipper containing ImageViews to display the different plates in order. These plates and demonstration pictures
are found in res/drawable. When you finish the demonstration you will be asked if you are ready and can move on to the test.

TestActivityHrr.java/activity_hrr_test.xml:
This is where the test is run. At create the plates and buttons are loaded. There are five showing buttons, these are a Circle, X, up/down triangle,
right/left triangle and next. There are also four hidden buttons behind the plate which will be used to mark the different figures being shown.
To get full score you will have to choose the showing figure aswell as the corresponding button before pressing next. There is also a hidden timer which
is explained in the demonstration. The timer will start when a plate is shown, the timer will count to 8 before switching to a new plate and register
the answer given by the user so far. If the user presses next before the timer runs out it will be reset and start at 8 again on the next plate.
The test runs until the last plate, when the user presses next or the timer runs out the result screen is loaded.

Plates.java:
This file contains all the needed code to flip through the plates used in the test. It flips through the plates and keeps count on what plate the user is on.
It also saves the answers given and calculates the result for the result screen. It holds all the answers for both the visable buttons and the hidden touch buttons
and makes them correspond.

ResultList.java/result_list.xml:
This activity contains text views for displaying the results calculated by the TestActivityHrr and Plates classes. This class writes out if you have any
colourvision defects and what extent your defects are, deutan, protan, tritan or tetertan. It also writes out your full score.


# What can be done in the future? #
There are no items on the TODO list made by the supervisor, but the group themselves have an idea of what could be good to add later on.
GroupsTODO:
- Change the touch buttons so that they have an anker in the middle and fills the rest of the screen (Group ran out of time on this)
- Add test that could be used on children (This will use only the first 6 plates and skip the rest)
- Fix new scans so that we don't get tilted pictures.

# What was easy and what was hard? #

The hardest part was understanding the HRR test, since there is very little information about it. But due to a lot of correspondanse with the supervisor through the
project we managed to get a good grasp of the wanted end result. He also provided us with the plates needed for the test as well as a picture of what each plate contains
so that we don't make a mistake on what was on each plate.
Another hard part was figuring out how to calculate the result, if the user had a defect, and to what extent. But this also was understood by the plate sent by the
supervisor and interpeted by us.

An easy part of the project was the design, since the project basically is a slideshow of pictures, it got more interesting when we had to correspond touch and buttons
to figures on the picture.

# Tests: #
There are no test methods made in the project. The tests were done by the group for many hours during the development.

# Report #
The report of the project can be found in the repository.
/report_colour_blindness_app.pdf